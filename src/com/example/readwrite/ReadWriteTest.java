package com.example.readwrite;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;

public class ReadWriteTest {
	
	/*input:内容，路径  （把内容写到指定文件）
	 * output：多个文件D://bigdata//test-270.txt，D://bigdata//train-9-30.txt
	 * */
	public void writerTxt(String content, String filePath) {   
		String filepath = filePath;
		BufferedWriter fw = null;
		try {
			File file = new File(filepath);
			fw = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(file, true), "UTF-8")); // 指定编码格式，以免读取时中文字符异常
			fw.append(content); 
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (fw != null) {
				try {
					fw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	} 

	/*对原数据进行格式处理
	 * intput：读的路径，flag 0，1分别对train，test做不同处理
	 * output：多出2个txt文件 内容，test-270.txt，train-9-30
	 *1. 把270块转换为270行
	 *2.把270行分为9类，加上类标 
	 * */
	public String readTxt(String filepath,int flag) { 
		String filePath = filepath;
		BufferedReader reader = null;
		double[] arr12 = new double[] {};

		String documentStr = "";
		int tmpSpeaker = 0;
		try {
			reader = new BufferedReader(new InputStreamReader(
					new FileInputStream(filePath), "UTF-8")); // 指定读取文件的编码格式，要和写入的格式一致，以免出现中文乱码,

			String str = null; 
			String mapValue = "";
			int mapKey = 0;
			int lineNum = 0;// 每块行数统计(全局变量！！！！！！！！！！！！！！！！！！！！)
			arr12 = new double[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };// 每块维统计(全局变量！！！！！！！！！！！！！！！！！！！！)
			while ((str = reader.readLine()) != null) {  
				if (str.length() != 0) { // 某行操作
					String[] strArr = str.split(" ");// 每维数据
					// 每块行数的累计
					lineNum++;
					// 每块每维累加之和
					for (int j = 0; j < 12; j++) { // strArr.length - 1 
						arr12[j] += Double.parseDouble(strArr[j]);// 每一维数字累加 
					} 
				}
				if (str.length() == 0) {// 上一块结束 
					for (int k = 0; k < 12; k++) {
						// 把求的每维平均数依旧放在数组每维的对应位置中 
						arr12[k] = arr12[k] / lineNum;
						BigDecimal bd = new BigDecimal(arr12[k]);
						// 依次累加每维内容
						mapValue += String.valueOf(bd.setScale(6,
								BigDecimal.ROUND_HALF_UP).doubleValue())
								+ " ";

					}
					mapKey++;// 换块，即接下来的一个12维计平均数
					lineNum = 0;// 每块的行号置0 
					if(flag==0){//train
						 //对于train要分（0），对于test（1）不需要
						 if(mapKey%30==0){
						 tmpSpeaker =mapKey/30;
						 }else{
						 tmpSpeaker =mapKey/30+1;
						 }
						documentStr += tmpSpeaker+" "+mapValue+"\r\n";//train
					}else{
						documentStr += mapKey + " " + mapValue + "\r\n";//test
					} 
					/*每次使用后对数组进行情况，值也清空，用于下一个*/
					arr12 = new double[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
					mapValue = ""; 
				}

			}
		 
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} 
		return documentStr;
	} 
 
}
